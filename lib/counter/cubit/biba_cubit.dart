import 'package:bloc/bloc.dart';

/// {@template counter_cubit}
/// A [Cubit] which manages an [int] as its state.
/// {@endtemplate}
///

class Biba {
  final String? str;
  final int? a;

  Biba({this.str, this.a});

  @override
  String toString() {
    return 'Biba: Biba.str:$str, Biba.a:$a';
  }

  copywith({String? string, int? i}) {
    return Biba(
      a: i ?? this.a,
      str: string ?? this.str,
    );
  }
}

// class CounterCubit extends Cubit<int> {
//   /// {@macro counter_cubit}
//   CounterCubit() : super(0);
//
//   /// Add 1 to the current state.
//   void increment() => emit(state + 1);
//
//   /// Subtract 1 from the current state.
//   void decrement() => emit(state - 1);
// }

class BibaCubit extends Cubit<Biba> {
  /// {@macro counter_cubit}
  BibaCubit()
      : super(
          Biba(a: 0, str: 'bibus'),
        );

  void increaseBiba() => emit(state.copywith(i: state.a! + 1));

  void decreaseBiba() => emit(state.copywith(i: state.a! - 1));
}
